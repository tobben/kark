//scale(100)
//translate([0,0,-.6])

difference()
{
    import("kark37.stl");
    translate([0.02,0,0])

    for(k=[0,1])
    mirror([0,k,0])
    translate([-0.43, 0.37, 0])
    rotate([3,8,0])
    translate([0, 0, -0.2])
    #cylinder(d=0.12, h=1.1, $fn=50);

    for(p=[1])
    mirror([p,0,0])
    for(k=[0,1])
    mirror([0,k,0])
    translate([-0.36, 0.37, 0])
    rotate([3,2,0])
    translate([0, 0, -0.2])
    #cylinder(d=0.12, h=1.1, $fn=50);
}
